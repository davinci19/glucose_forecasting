import numpy as np
import pandas as pd
import json
import pickle
from datetime import datetime, time, date
from copy import deepcopy

DATE_FMT = "%d-%m-%Y"
TIME_FMT = "%H:%M:%S"
DT_FMT = "%Y-%m-%dT%H:%M:%S"

meal_data = json.load(open("final_meal_data.json", "r"))
sugar_data = pickle.load(open("sugars_for_meal_patients.pickle", "rb"))
steps_data = pd.read_csv("steps_data.csv")


def preprocess2(data):
    # Check datetime.utcfromtimestamp() if UTC time is to be used instead of local
    dates2, times2, data2 = zip(
        *[
            (
                str(datetime.fromtimestamp(x["timestamp"]).date()),
                str(datetime.fromtimestamp(x["timestamp"]).time()),
                x["glucose"],
            )
            for x in data
        ]
    )

    datetime2 = {}
    cur = 0
    for i in range(1, len(dates2)):
        if dates2[i] != dates2[cur]:
            datetime2[dates2[cur]] = (cur, i - 1)
            cur = i
        elif i == len(dates2) - 1:
            datetime2[dates2[cur]] = (cur, i)

    # print(datetime2)
    dates2 = np.unique(dates2)
    # print(dates2)
    return dict(dates=dates2, times=times2, datetime=datetime2, glucose=data2)


def datetime_to_millis(meal_records):
    for record in meal_records:
        record["timestamp"] = datetime.strptime(
            record["date"] + " " + record["mealTime"], DATE_FMT + " " + TIME_FMT
        ).timestamp()
    return meal_records


def temp_func(steps, sugar_records):
    for j in range(len(sugar_records)):
        if steps["timestamp"] <= sugar_records[j]["timestamp"]:
            steps.pop("timestamp")
            return j, {**sugar_records[j], **steps}
    return 0, 0


def attach_meals(meal_records, sugar_records):
    new_records = deepcopy(sugar_records)
    sugar_records = sorted(sugar_records, key=lambda x: x["timestamp"])
    for i in range(len(meal_records)):
        j, temp = temp_func(meal_records[i], sugar_records)
        new_records[j] = temp
    return new_records


def process_steps(steps_df):
    # _steps = steps[steps['patient_profile_id'] == int(PROFILE)]
    _steps = steps_df[["steps", "created_time"]].copy()

    dates = np.unique(
        [
            datetime.strptime(x.split(".")[0], DT_FMT).date()
            for x in list(_steps["created_time"])
        ]
    )
    # print(dates)

    steps2 = {k: {} for k in dates}

    for _date in steps2:
        steps2[_date] = {x: [] for x in range(24)}

    steps_info = []
    for index, row in _steps.iterrows():
        dt = datetime.strptime(row["created_time"].split(".")[0], DT_FMT)
        # steps_info.append({'timestamp': dt.timestamp(), 'steps': row['steps']})
        steps2[dt.date()][dt.hour].append(row["steps"])
    for _date in steps2:
        for _time in steps2[_date]:
            steps2[_date][_time] = np.sum(steps2[_date][_time]).astype("int32")

    for _date in steps2:
        for _time in steps2[_date]:
            ts = datetime.combine(_date, time(int(_time))).timestamp()
            steps_info.append({"timestamp": ts, "steps": steps2[_date][_time]})

    return steps_info


def attach_steps(steps_record, sugar_records):
    # print("sugar_len:", len(sugar_records))
    new_records = deepcopy(sugar_records)
    sugar_records = sorted(sugar_records, key=lambda x: x["timestamp"])
    for i in range(len(steps_record)):
        # print(steps_record[i])
        j, temp = temp_func(steps_record[i], sugar_records)
        if temp != 0 and j != 0:
            new_records[j] = temp
    return new_records


# Create Temporal Data
# Match Meal times with sugar readings, ie put meal to nearest sugar reading

selected_patients = [x for x in meal_data if len(meal_data[x]) > 18]
print("Filter : ", len(selected_patients))


for patient in selected_patients:
    meal_records = datetime_to_millis(meal_data[patient])
    final_patient_data = attach_meals(meal_records, sugar_data[patient])
    patient_steps_df = steps_data[steps_data["patient_profile_id"] == int(patient)]
    patient_steps_record = process_steps(patient_steps_df)
    # print("here:",len(final_patient_data))

    if len(patient_steps_record) > 0:
        final_patient_data = attach_steps(patient_steps_record, final_patient_data)
    else:
        print("Couldn't attach steps for :", patient)
    pd.DataFrame(final_patient_data).to_csv(str(patient) + ".csv")

pickle.dump(selected_patients, open("selected_patients.pickle", "wb"))
